# My NixOS Dotfiles

This is my current multi-system NixOS configuration.

## 🎨 Desktop Aesthetic

Hyprland, Haskell-scripted EWW bar, and some AI art I made

![Desktop](./assets/desktop.png)

Helix, WezTerm, Starship, and Nushell

![Desktop](./assets/helix.png)

## 📁 File-Structure

### `home-manager/`

configurations for various programs through Home Manager

### `lib/`

functions for building NixOS configurations and Home Manager configurations

### `nixOS/`

configurations for various NixOS systems

### `parts/`

useful flake tools

### `secrets/`

private data hidden with SOPS

### `templates/`

my various development environment templates

### `themes/`

theme data to be called throughout the configurations

### `users/`

user data to be called throughout the configurations
 
### `flake.nix`

entrypoint to the system configurations
