# Since it's just me there's only one home for now. Could build more though. Also don't blend this
# with users because homes/users aren't bijective.
{
  flake,
  ...
}:
let
  inherit (flake.config.people) myself;
in
{
  home = {
    homeDirectory = "/home/${myself}";
    sessionVariables = { };
    username = myself;
  };
}
