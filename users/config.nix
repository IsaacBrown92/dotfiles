let
  myself = "isaac";
in
{
  inherit myself;
  users = {
    "${myself}" = {
      domain = "nextcloud.askyourself.ca";
      name = "Isaac Brown";
      email = "isaacbrown92@outlook.com";
      sshKeys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKogc+5HBh1GSsEN1vqTchxRh0ERCJ6PmQnr+X7efhdB isaac@desktop"
      ];
    };
  };
}
