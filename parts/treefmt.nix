{
  treefmt = {
    flakeCheck = false;
    programs = {
      nixfmt.enable = true;
    };
    projectRootFile = "flake.nix";
  };
}
