{
  config,
  pkgs,
  ...
}:
{
  devShells = {
    default = pkgs.mkShell {
      inputsFrom = [
        config.pre-commit.devShell
        config.treefmt.build.devShell
      ];
      packages = builtins.attrValues {
        inherit (pkgs)
          dhall
          dhall-json
          dhall-lsp-server
          gource
          just
          nil
          nixfmt-rfc-style
          pre-commit
          sops
          ssh-to-age
          vscode-langservers-extracted
          ;
      };
    };
  };
}
