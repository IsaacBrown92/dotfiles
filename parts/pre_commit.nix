{
  pre-commit.settings.hooks = {
    convco.enable = true;
    nil.enable = true;
    statix.enable = true;
    treefmt.enable = true;
  };
}
