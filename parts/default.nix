{ inputs, ... }:
{
  imports = [
    inputs.pre-commit-hooks-nix.flakeModule
    inputs.treefmt-nix.flakeModule
  ];
  perSystem = {
    imports = [
      ./devshells.nix
      ./pre_commit.nix
      ./treefmt.nix
    ];
  };
}
