makePickles:
	mkdir -p ./test/1/pickles
	mkdir -p ./test/2/pickles
	mkdir -p ./test/3/pickles
		
deleteEWWLogs:
	doas rm -r $HOME/.cache/eww*

deleteMozillaCache:
	doas rm -r $HOME/.mozilla

deleteNextcloudCache:
	doas rm -r /var/lib/nextcloud

deleteNewsboatCache:
	doas rm -r $HOME/.local/share/newsboat
	
deletePostgreSQLCache:
	doas rm -r /var/lib/postgresql
	
deleteTofiCache:
	doas rm -r $HOME/.cache/tofi*

eww ARGS:
	eww -c ~/Dotfiles/home-manager/modules/eww/config {{ARGS}}

updateMoonlander:
    curl -k -L https://oryx.zsa.io/3aVbB/latest/binary -o layout.bin
    wally-cli layout.bin
    rm layout.bin
