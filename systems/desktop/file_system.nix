{
  services.davfs2.enable = true;

  fileSystems = {
    "/boot/efi" = {
      device = "/dev/disk/by-uuid/CF91-9759";
      fsType = "vfat";
    };
    "/" = {
      device = "/dev/disk/by-uuid/f072408c-4c1a-49ed-8513-6d626451f9d9";
      fsType = "ext4";
    };
    "/home/isaac/Media/6t-drive" = {
      device = "/dev/disk/by-uuid/ECF299EDF299BBF0";
      fsType = "ntfs";
    };
    "/home/isaac/Media/pass-usb" = {
      device = "/dev/disk/by-uuid/7FBF-F2A3";
      fsType = "exfat";
      options = [
        "auto"
        "defaults"
        "gid=100"
        "nofail"
        "uid=1000"
      ];
    };
    # "/home/isaac/Media/ventoy" = {
    #   device = "/dev/disk/by-uuid/8482-F915";
    #   fsType = "exfat";
    #   options = [
    #     "auto"
    #     "defaults"
    #     "gid=100"
    #     "nofail"
    #     "uid=1000"
    #     "x-systemd.automount"
    #     "x-systemd.device-timeout=5s"
    #   ];
    # };
    "/home/isaac/Media/1t-drive" = {
      device = "/dev/disk/by-uuid/76E8-CACF";
      fsType = "exfat";
      options = [
        "auto"
        "defaults"
        "gid=100"
        "nofail"
        "uid=1000"
        "x-systemd.automount"
        "x-systemd.device-timeout=2s"
      ];
    };
  };
}
# There's a tool called udiskie that does automounting well and should be used for all
# optional devices. Switch when you have time.
