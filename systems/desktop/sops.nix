{ flake, ... }:
let
  me = flake.config.people.myself;
in
{
  sops = {
    age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    defaultSopsFile = ../../secrets/desktop.yml;
    secrets = {
      "anthropic-key" = {
        owner = me;
        # path = "/home/${me}/Documents/keys/anthropic-key.txt";
      };
      "davfs2-secrets" = {
        mode = "600";
        owner = "root";
        path = "/var/secrets/davfs2-secrets";
      };
      "email1" = {
        path = "/var/secrets/email1";
      };
      "open-weather-map-api-key" = {
        owner = me;
        path = "/home/${me}/Documents/keys/open-weather-map-api-key.txt";
      };
      "ssh-private-key" = {
        owner = "root";
        path = "/var/secrets/ssh-private-key";
      };
    };
  };
}
