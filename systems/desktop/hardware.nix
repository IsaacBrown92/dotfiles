{
  config,
  lib,
  modulesPath,
  pkgs,
  ...
}:
{
  hardware = {
    cpu.amd.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
    graphics.enable32Bit = true;
    keyboard.zsa.enable = true;
    ledger.enable = true;
  };

  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];

  nixpkgs.hostPlatform = "x86_64-linux";

  services.udev = {
    enable = true;
    packages = [ pkgs.android-udev-rules ];
  };

  system.stateVersion = "22.05";
}
