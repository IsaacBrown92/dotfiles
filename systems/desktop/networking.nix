{ lib, ... }:
{
  networking = {
    firewall = {
      allowedUDPPorts = [ 1900 ];
      allowedUDPPortRanges = [
        {
          from = 32768;
          to = 61000;
        }
      ];
    };
    hostName = "desktop";
    networkmanager.enable = true;
    useDHCP = lib.mkDefault true;
  };
}
