{
  imports = [
    ./boot.nix
    ./file_system.nix
    ./hardware.nix
    ./networking.nix
    ./sops.nix
  ];
  programs.dconf.enable = true;
}
