{ flake, ... }:
let
  me = flake.config.people.myself;
in
{
  sops = {
    age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    defaultSopsFile = ../../../secrets/laptop.yml;
    secrets = {
      "open-weather-map-api-key" = {
        owner = "isaac";
        path = "/home/${me}/Documents/keys/open-weather-map-api-key.txt";
      };
    };
  };
}
