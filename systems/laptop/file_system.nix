{
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/99f2c4ad-a832-48a3-9586-14d399dc4043";
      fsType = "ext4";
    };
    "/boot/efi" = {
      device = "/dev/disk/by-uuid/4EF9-7F0E";
      fsType = "vfat";
    };
    "/home/isaac/Media/1t-drive" = {
      device = "/dev/disk/by-uuid/76E8-CACF";
      fsType = "exfat";
      options = [
        "defaults"
        "nofail"
        "user"
      ];
    };
    "/home/isaac/Media/exam-center-drive" = {
      # device = "/dev/disk/by-uuid/00FE-4C12";
      device = "/dev/disk/by-uuid/0CA3-D7FB";
      fsType = "vfat";
      options = [
        "defaults"
        "nofail"
        "user"
      ];
    };
  };
}
