{
  imports = [
    ./boot.nix
    ./file_system.nix
    ./hardware.nix
    ./networking.nix
    ./ssh.nix
  ];
}
