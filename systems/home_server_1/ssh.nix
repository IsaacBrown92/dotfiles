{ flake, ... }:
let
  inherit (flake.config.people) myself;
in
{
  users.users.${myself} = {
    openssh.authorizedKeys.keys = flake.config.people.users.${myself}.sshKeys ++ [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILrN+tLxImC5Y6jGjSkhf2lGVUWp3m00r+7kM/eZA0ON sofia"
    ];
  };
}
