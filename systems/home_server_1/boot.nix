{ pkgs, ... }:
{
  boot = {
    initrd.availableKernelModules = [
      "uhci_hcd"
      "ehci_pci"
      "ata_piix"
      "hpsa"
      "usb_storage"
      "usbhid"
      "sd_mod"
      "sr_mod"
    ];

    kernelModules = [ "kvm-intel" ];

    kernelPackages = pkgs.linuxPackages_latest;

    loader.grub = {
      device = "/dev/sda";
      enable = true;
    };
  };
}
