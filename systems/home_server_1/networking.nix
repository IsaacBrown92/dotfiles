{ lib, ... }:
{
  networking = {
    hostName = "home-server-1";
    networkmanager.enable = true;
    useDHCP = lib.mkDefault true;
  };

  services.sshd.enable = true;
}
