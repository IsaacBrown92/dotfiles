{ config, ... }:
{
  sops = {
    age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    defaultSopsFile = ../../../secrets/home-server-2.yml;
    secrets = {
      "nextcloud-password" = {
        mode = "0440";
        owner = "nextcloud";
        group = "nextcloud";
      };
      "website-ssl-cert" = {
        owner = config.services.nginx.user;
      };
      "website-ssl-key" = {
        owner = config.services.nginx.user;
      };
    };
  };
}
