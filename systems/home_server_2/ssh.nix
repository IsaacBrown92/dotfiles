{ flake, ... }:
let
  inherit (flake.config.people) myself;
in
{
  users.users.${myself} = {
    openssh.authorizedKeys.keys = flake.config.people.users.${myself}.sshKeys;
  };
}
