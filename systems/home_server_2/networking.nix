{ lib, ... }:
{
  networking = {
    firewall.allowedTCPPorts = [
      80
      443
    ];
    hostName = "home-server-2";
    networkmanager.enable = true;
    useDHCP = lib.mkDefault true;
  };

  services.sshd.enable = true;
}
