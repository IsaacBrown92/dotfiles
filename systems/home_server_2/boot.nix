{
  lib,
  pkgs,
  ...
}:
{
  boot = {
    initrd.availableKernelModules = [
      "uhci_hcd"
      "ehci_pci"
      "ata_piix"
      "hpsa"
      "usb_storage"
      "usbhid"
      "sd_mod"
      "sr_mod"
    ];

    kernelModules = [ "kvm-intel" ];

    kernelPackages = pkgs.linuxPackages_latest;

    loader.grub = {
      device = "/dev/disk/by-id/wwn-0x600508b1001ca20d9c5ad8f472906cff";
      enable = true;
      # useOSProber = true;
    };

    swraid.enable = lib.mkForce false;
  };
}
