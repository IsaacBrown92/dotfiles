{
  description = "Hydra";

  inputs = {
    # claude-desktop-app.url = "github:0x50F1A/claude-desktop-linux-flake";
    edgedb = {
      url = "github:edgedb/packages-nix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-parts.follows = "flake-parts";
    };
    firefox-addons.url = "gitlab:rycee/nur-expressions?dir=pkgs/firefox-addons";
    home-manager.url = "github:nix-community/home-manager";
    lix-module = {
      url = "https://git.lix.systems/lix-project/nixos-module/archive/2.92.0.tar.gz";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixpkgs-gtk.url = "github:NixOS/nixpkgs/85b081528b937df4bfcaee80c3541b58f397df8b";
    nixpkgs-stable.url = "github:NixOS/nixpkgs/nixos-23.11";
    nixvim.url = "github:nix-community/nixvim";
    pre-commit-hooks-nix.url = "github:cachix/pre-commit-hooks.nix";
    sops-nix.url = "github:Mic92/sops-nix";
    treefmt-nix = {
      url = "github:numtide/treefmt-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    inputs@{
      flake-parts,
      lix-module,
      nixvim,
      self,
      ...
    }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      imports = [
        inputs.pre-commit-hooks-nix.flakeModule
        ./lib
        ./modules/home_manager
        ./modules/nixos
        ./parts
        ./users
        ./themes
      ];

      systems = [ "x86_64-linux" ];
      flake =
        { config, ... }:
        {
          homeConfigurations = {
            desktop = self.lib.mkHome {
              modules = [
                ./homes
                { home.stateVersion = "22.05"; }
                config.homeModules.commandline
                config.homeModules.communication
                config.homeModules.desktop
                config.homeModules.editing
                config.homeModules.finance
                config.homeModules.gaming
                config.homeModules.graphicalBase
                config.homeModules.internet
                config.homeModules.media
                config.homeModules.programming
                config.homeModules.productivity
                config.homeModules.services
                nixvim.homeManagerModules.nixvim
              ];
            };
            laptop = self.lib.mkHome [
              ./homes
              config.homeModules.commandline
              config.homeModules.communication
              config.homeModules.graphicalBase
              config.homeModules.internet
              config.homeModules.laptop
              config.homeModules.media
              config.homeModules.programming
              config.homeModules.productivity
              config.homeModules.services
            ];

            server = self.lib.mkHome [
              ./homes
              config.homeModules.commandline
            ];
          };
          nixosConfigurations = {
            desktop = self.lib.mkLinuxSystem [
              ./systems/desktop
              config.nixosModules.base
              config.nixosModules.graphicalBase
              config.nixosModules.desktop
              lix-module.nixosModules.default
              inputs.sops-nix.nixosModules.sops
            ];

            laptop = self.lib.mkLinuxSystem [
              ./systems/laptop
              config.nixosModules.base
              config.nixosModules.graphicalBase
              config.nixosModules.laptop
              lix-module.nixosModules.default
              inputs.sops-nix.nixosModules.sops
            ];

            homeServer1 = self.lib.mkLinuxSystem [
              ./systems/home_server_1
              config.nixosModules.base
              config.nixosModules.homeServer1
              lix-module.nixosModules.default
              inputs.sops-nix.nixosModules.sops
            ];

            homeServer2 = self.lib.mkLinuxSystem [
              ./systems/home_server_2
              config.nixosModules.base
              config.nixosModules.homeServer2
              lix-module.nixosModules.default
              inputs.sops-nix.nixosModules.sops
            ];
          };

          templates = {
            c = {
              path = ./templates/c;
              description = "C Environment";
            };
            dhall = {
              path = ./templates/dhall;
              description = "Dhall Environment";
            };
            haskell = {
              path = ./templates/haskell;
              description = "Haskell Environment";
            };
            java = {
              path = ./templates/java;
              description = "Java Environment";
            };
            latex = {
              path = ./templates/latex;
              description = "Latex Environment";
            };
            r = {
              path = ./templates/r;
              description = "R Environment";
            };
            typst = {
              path = ./templates/typst;
              description = "Typst Environment";
            };
          };
        };
    };
}
