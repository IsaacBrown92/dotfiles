{
  pkgs,
  config,
  ...
}:
{
  devShells.default = pkgs.mkShell {
    packages = builtins.attrValues {
      inherit (pkgs)
        alejandra
        clang-tools
        nil
        valgrind
        ;
      inherit (pkgs.nodePackages) "@commitlint/config-conventional";
    };

    shellHook = "${config.pre-commit.installationScript}";
  };
}
