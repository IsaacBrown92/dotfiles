{
  pkgs,
  config,
  ...
}:
{
  devShells.default = pkgs.mkShell {
    packages = builtins.attrValues {
      inherit (pkgs)
        alejandra
        gradle
        jdk22
        jdt-language-server
        just
        nil
        yamlfmt
        ;
      inherit (pkgs.nodePackages) "@commitlint/config-conventional";
    };

    shellHook = "${config.pre-commit.installationScript}";
  };
}
