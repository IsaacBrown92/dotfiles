{
  pkgs,
  config,
  ...
}:
{
  devShells.default = pkgs.mkShell {
    packages = builtins.attrValues {
      inherit (pkgs)
        alejandra
        just
        nil
        typst
        tinymist
        typstyle
        yamlfmt
        ;
      inherit (pkgs.nodePackages) "@commitlint/config-conventional";
    };

    shellHook = "${config.pre-commit.installationScript}";
  };
}
