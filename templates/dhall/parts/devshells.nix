{
  pkgs,
  config,
  ...
}:
{
  devShells.default = pkgs.mkShell {
    packages = builtins.attrValues {
      inherit (pkgs)
        alejandra
        dhall
        dhall-json
        dhall-lsp-server
        nil
        yamlfmt
        ;
      inherit (pkgs.nodePackages) "@commitlint/config-conventional";
    };

    shellHook = "${config.pre-commit.installationScript}";
  };
}
