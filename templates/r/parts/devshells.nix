{
  pkgs,
  config,
  ...
}:
{
  devShells.default = pkgs.mkShell {
    packages = builtins.attrValues {
      inherit (pkgs)
        alejandra
        nil
        ;
      inherit (pkgs.rPackages) tidyverse;
      inherit (pkgs.nodePackages) "@commitlint/config-conventional";
    };

    shellHook = "${config.pre-commit.installationScript}";
  };
}
