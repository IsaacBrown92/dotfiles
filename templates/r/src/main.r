library(tidyverse)

set.seed(123)
data <- data.frame(
  x = rnorm(100),
  y = 2 * x + rnorm(100)
)

ggplot(data, aes(x = x, y = y)) +
  geom_point() +
  labs(title = "Scatter Plot", x = "X-axis", y = "Y-axis") +
  theme_minimal()
