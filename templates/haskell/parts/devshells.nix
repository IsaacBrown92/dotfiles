{
  pkgs,
  config,
  ...
}:
{
  haskellProjects.default = {
    devShell = {
      enable = true;

      tools = hp: {
        inherit (hp)
          cabal-fmt
          haskell-language-server
          ;
        inherit (pkgs)
          alejandra
          just
          nil
          ;
      };

      hlsCheck.enable = true;

      mkShellArgs.shellHook = "${config.pre-commit.installationScript}";
    };
  };
}
