{
  inputs,
  config,
  self,
  ...
}:
let
  specialArgs = {
    inherit inputs;
    flake = { inherit self inputs config; };
  };
in
{
  flake.lib = {
    mkHome =
      {
        modules,
        pkgs ? import inputs.nixpkgs {
          config.allowUnfree = true;
          # overlays = [ inputs.claude-desktop-app.overlays.default ];
          system = "x86_64-linux";
        },
      }:
      inputs.home-manager.lib.homeManagerConfiguration {
        inherit modules pkgs;
        extraSpecialArgs = specialArgs;
      };
    mkLinuxSystem =
      modules:
      inputs.nixpkgs.lib.nixosSystem {
        inherit modules specialArgs;
      };

    mkModuleList =
      dir:
      builtins.listToAttrs (
        map (file: {
          name = builtins.head (builtins.split "\\..*" file);
          value = import (dir + "/${file}");
        }) (builtins.filter (name: name != "default.nix") (builtins.attrNames (builtins.readDir dir)))
      );
  };
}
