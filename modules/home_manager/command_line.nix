{ pkgs, ... }:
{
  home.packages = builtins.attrValues {
    inherit (pkgs)
      alacritty
      claude-code
      bzip2
      diceware
      dig
      fastfetch
      file
      flac
      ffmpeg
      fzf
      inetutils
      iperf
      kalker
      killall
      mkp224o
      ncdu
      nmap
      numbat
      openssl
      pinentry-curses
      # pynitrokey
      qrencode
      # steghide
      s-tui
      toipe
      tokei
      tomb
      tree
      unrar
      unzip
      ventoy
      xterm
      zip
      ;
    # inherit (pkgs.plover) dev;
  };
}
