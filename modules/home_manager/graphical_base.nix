{ pkgs, ... }:
{
  home.packages = builtins.attrValues {
    inherit (pkgs.kdePackages) okular;
    inherit (pkgs.xfce) thunar;
  };
}
