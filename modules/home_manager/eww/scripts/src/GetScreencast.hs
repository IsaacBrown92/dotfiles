{-# OPTIONS_GHC -Wall #-}

import Data.Char (digitToInt)
import EWWLib (readHyprlandSocketOutput)
import Text.Parsec (digit, string)
import Text.Parsec.String (Parser)
import System.IO (hSetBuffering, stdout, BufferMode (LineBuffering))

main :: IO ()
main = do 
  hSetBuffering stdout LineBuffering
  readHyprlandSocketOutput screencastParser updateRecWidgetClass

screencastParser :: Parser (Maybe Int)
screencastParser = do
  _ <- string "screencast>>"
  Just . digitToInt <$> digit

updateRecWidgetClass :: Maybe Int -> IO ()
updateRecWidgetClass maybeState = putStrLn $ case maybeState of
  Just 1 -> "true"
  _ -> "false"