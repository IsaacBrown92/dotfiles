{-# LANGUAGE BlockArguments #-}
{-# OPTIONS_GHC -Wall #-}

import Data.Foldable (traverse_)
import Data.List (sort)
import EWWLib (Monitor (monActiveWSID, monID), Workspace (wsID), getMons, getWSs, readHyprlandSocketOutput)
import System.IO (BufferMode (LineBuffering), hSetBuffering, stdout)
import Text.Parsec (string)
import Text.Parsec.String (Parser)

main :: IO ()
main = do
  hSetBuffering stdout LineBuffering
  readHyprlandSocketOutput wsParser updateWSs

wsParser :: Parser (Maybe a)
wsParser = do
  _ <- string "workspace>>"
  pure Nothing

updateWSs :: Maybe a -> IO ()
updateWSs _ = do
  maybeMons <- getMons
  maybeWSs <- getWSs
  traverse_ putStrLn $ makeUpdateCommand <$> maybeMons <*> maybeWSs

makeUpdateCommand :: [Monitor] -> [Workspace] -> String
makeUpdateCommand mons wss = "(box :spacing 4 :vexpand true " ++ overlays ++ ")"
  where
    overlays = concatMap (makeOverlay mons) $ sort wss

makeOverlay :: [Monitor] -> Workspace -> String
makeOverlay mons ws =
  "(overlay :width 42 (label :class \"ws\" :text \""
    ++ toRoman (wsID ws)
    ++ "\") (label :class \""
    ++ monLabelClass
    ++ "\" :valign \"end\" :text \""
    ++ monLabelText
    ++ "\"))"
  where
    (monLabelClass, monLabelText) = case getActiveMonID ws (getMonActiveWSPairs mons) of
      Just monID -> ("monLabel", 'M' : show (monID + 1))
      Nothing -> ("monLabelHidden", "")

toRoman :: Int -> String
toRoman int = case int of
  1 -> "Ⅰ"
  2 -> "Ⅱ"
  3 -> "Ⅲ"
  4 -> "Ⅳ"
  5 -> "Ⅴ"
  6 -> "Ⅵ"
  7 -> "Ⅶ"
  8 -> "Ⅷ"
  9 -> "Ⅸ"
  10 -> "Ⅹ"
  11 -> "Ⅺ"
  12 -> "Ⅻ"
  _ -> "∄"

getActiveMonID :: Workspace -> [(Int, Int)] -> Maybe Int
getActiveMonID ws monActiveWSPairs' = case monActiveWSPairs' of
  (monID, activeWSID) : xs
    | activeWSID == wsID ws -> Just monID
    | otherwise -> getActiveMonID ws xs
  _ -> Nothing

getMonActiveWSPairs :: [Monitor] -> [(Int, Int)]
getMonActiveWSPairs mons' = case mons' of
  x : xs -> (monID x, monActiveWSID x) : getMonActiveWSPairs xs
  _ -> []