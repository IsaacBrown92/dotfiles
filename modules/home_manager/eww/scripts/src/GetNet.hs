{-# LANGUAGE QuasiQuotes #-}
{-# OPTIONS_GHC -Wall #-}

import Network.HostName (getHostName)
import Text.RawString.QQ (r)

main :: IO ()
main = do
  hostname <- getHostName
  let netMagicVar = case hostname of
        "desktop" -> "enp6s0"
        _ -> "wlo1"
  putStrLn $
    [r|
(box
  :class "graphBox"
  :orientation "vertical"
  :tooltip "${EWW_NET.|]
      ++ netMagicVar
      ++ [r|.NET_DOWN}B/s"
  :vexpand true
  :width 60
  (graph    
    :class "netGraph"
    :thickness 3
    :time-range "60s"
    :value {EWW_NET.|]
      ++ netMagicVar
      ++ [r|.NET_DOWN}
    :vexpand true
  )
  (label :text "net")
)|]