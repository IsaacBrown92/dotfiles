{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wall #-}

import Control.Lens ((^?!))
import Data.Aeson.Key (fromString)
import Data.Aeson.Lens (key, nth, _Double, _String)
import Data.Text (unpack)
import Data.Yaml (Value)
import EWWLib (update)
import Network.HTTP.Client (Request, parseRequest)
import Network.HTTP.Simple (getResponseBody, httpJSON)
import System.Process (callProcess)

main :: IO ()
main = do
  apiKey <- readFile "/home/isaac/Documents/keys/open-weather-map-api-key.txt"
  response <- httpJSON $ getWeather apiKey
  let iconCode = unpack $ (getResponseBody response :: Value) ^?! key (fromString "weather") . nth 0 . key "icon" . _String
      temp = (getResponseBody response :: Value) ^?! key (fromString "main") . key "temp" . _Double
  callProcess "eww" $
    update
      [ ("weatherIcon", "/home/isaac/.config/eww/images/weather/" ++ getIcon iconCode ++ ".png"),
        ("temp", show temp ++ "°")
      ]

getWeather :: String -> Request
getWeather apiKey =
  case parseRequest url of
    Left err -> error $ "Failed to parse request: " ++ show err
    Right req -> req
  where
    url = "http://api.openweathermap.org/data/2.5/weather?APPID=" ++ apiKey ++ "&id=5967629&units=metric"

getIcon :: String -> String
getIcon iconCode = case iconCode of
  "01d" -> "clearSkyDay"
  "01n" -> "clearSkyNight"
  "02d" -> "fewCloudsDay"
  "02n" -> "fewCloudsNight"
  "03d" -> "clouds"
  "03n" -> "clouds"
  "04d" -> "clouds"
  "04n" -> "clouds"
  "09d" -> "showerRainDay"
  "09n" -> "showerRainNight"
  "10d" -> "rain"
  "10n" -> "rain"
  "11d" -> "thunderstorm"
  "11n" -> "thunderstorm"
  "13d" -> "snow"
  "13n" -> "snow"
  "50d" -> "mist"
  "50n" -> "mist"
  _ -> ""