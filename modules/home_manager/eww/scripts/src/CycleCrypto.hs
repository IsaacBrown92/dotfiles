{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wall #-}

import Control.Lens ((^?!))
import Data.Aeson.Key (fromString)
import Data.Aeson.Lens (key, _Double)
import Data.Char (toLower)
import Data.List (find)
import Data.Yaml (Value)
import EWWLib (getVariableValue, update)
import Network.HTTP.Client (Request (host, path, queryString), parseRequest)
import Network.HTTP.Simple (getResponseBody, httpJSON)
import System.Process (callProcess)

main :: IO ()
main = do
  val <- getVariableValue "cryptoIcon"
  case cycleCrypto val of
    Just crypto -> do
      price <- maybe "N/A" show <$> getPrice crypto
      callProcess "eww" $
        update
          [ ("cryptoIcon", iconPath crypto),
            ("price", price ++ " CAD")
          ]
    Nothing -> pure ()

cycleCrypto :: String -> Maybe Crypto
cycleCrypto str = do
  x <- find ((== str) . iconPath) cryptos
  case dropWhile (/= x) $ cycle cryptos of
    _ : next : _ -> Just next
    _ -> Nothing

getPrice :: Crypto -> IO (Maybe Double)
getPrice c = case [x | x <- cryptos, x == c] of
  [x] -> do
    let request = api x
    response <- httpJSON (cryptoRequestToRequest request)
    let price = (getResponseBody response :: Value) ^?! key (fromString $ toLower <$> name x) . key "cad" . _Double
    pure $ Just price
  _ -> pure Nothing

cryptoRequestToRequest :: CryptoRequest -> Request
cryptoRequestToRequest (CryptoRequest req) = req

makeCrypto :: String -> Crypto
makeCrypto str =
  case parseRequest url of
    Left err -> error $ "Failed to parse request: " ++ show err
    Right req ->
      Crypto
        { api = CryptoRequest req,
          iconPath = iconPath',
          name = str
        }
  where
    url = "https://api.coingecko.com/api/v3/simple/price?ids=" ++ str ++ "&vs_currencies=cad"
    iconPath' = "/home/isaac/.config/eww/images/crypto/" ++ (toLower <$> str) ++ ".png"

-- Data:
cryptos :: [Crypto]
cryptos =
  map
    makeCrypto
    [ "Cardano",
      "Bitcoin",
      "Encoins",
      "Ethereum",
      "Orcfax"
    ]

-- Datatypes:
data Crypto = Crypto
  { api :: CryptoRequest,
    iconPath :: String,
    name :: String
  }
  deriving (Show, Eq)

newtype CryptoRequest = CryptoRequest Request
  deriving (Show)

instance Eq CryptoRequest where
  (==) :: CryptoRequest -> CryptoRequest -> Bool
  (CryptoRequest req1) == (CryptoRequest req2) =
    (host req1, path req1, queryString req1)
      == (host req2, path req2, queryString req2)