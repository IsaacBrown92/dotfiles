{-# OPTIONS_GHC -Wall #-}

import EWWLib (update)
import Network.HostName (getHostName)
import System.Process (callProcess)

main :: IO ()
main = do
  hostname <- getHostName
  case hostname of
    "laptop" -> callProcess "eww" . update $ [("isBatVisible", "true")]
    _ ->
      pure ()