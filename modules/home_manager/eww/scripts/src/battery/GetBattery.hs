{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# OPTIONS_GHC -Wall #-}

import Data.Bool (bool)
import Data.Maybe (fromMaybe)
import System.Process (readProcess)
import Text.RawString.QQ (r)

main :: IO ()
main = do
  status <- readProcess "cat" ["/sys/class/power_supply/BAT0/status"] ""
  capacity <- readProcess "cat" ["/sys/class/power_supply/BAT0/capacity"] ""
  let capacity' = read capacity :: Double
  putStrLn . getUpdateCommand . getIconAndClass . Bat capacity' $ filter (/= '\n') status

getUpdateCommand :: (String, String) -> String
getUpdateCommand (iconText, iconClass) =
  [r|
(label
  :class "|]
    ++ iconClass
    ++ [r|"
  :text |]
    ++ iconText
    ++ [r|
  :vexpand true
)|]

getIconAndClass :: Bat -> (String, String)
getIconAndClass bat
  | totalAvg bat <= 10 = (batCh "0", yellowIfNot "batTextRedPad")
  | totalAvg bat <= 20 = (batCh "10", yellowIfNot "batTextRed")
  | totalAvg bat <= 30 = (batCh "20", yellowIfNot "batTextGreen")
  | totalAvg bat <= 40 = (batCh "30", yellowIfNot "batTextGreen")
  | totalAvg bat <= 50 = (batCh "40", yellowIfNot "batTextGreen")
  | totalAvg bat <= 60 = (batCh "50", yellowIfNot "batTextGreen")
  | totalAvg bat <= 70 = (batCh "60", yellowIfNot "batTextGreen")
  | totalAvg bat <= 80 = (batCh "70", yellowIfNot "batTextGreen")
  | totalAvg bat <= 90 = (batCh "80", yellowIfNot "batTextGreen")
  | totalAvg bat < 100 = (batCh "90", yellowIfNot "batTextGreen")
  | otherwise = (batCh "100", yellowIfNot "batTextGreen")
  where
    isCharging
      | status bat == "Discharging" = False
      | otherwise = True
    batCh num = "bat" ++ bool [] "Ch" isCharging ++ num
    colIfNotCharging col = bool (Just col) Nothing isCharging
    yellowIfNot col = fromMaybe "batTextYellow" $ colIfNotCharging col

-- Datatypes:
data Bat = Bat
  { totalAvg :: Double,
    status :: String
  }
  deriving (Show)