{-# OPTIONS_GHC -Wall #-}

import EWWLib (update)
import System.Environment (getArgs)
import System.Process (callProcess)

main :: IO ()
main = do
  args <- getArgs
  callProcess "eww" . update $
    case args of
      ["false"] ->
        [ ("power", "true"),
          ("powermenuEventboxClass", "powermenuEventboxOpen")
        ]
      _ ->
        [ ("power", "false"),
          ("powermenuEventboxClass", "powermenuEventboxClosed")
        ]