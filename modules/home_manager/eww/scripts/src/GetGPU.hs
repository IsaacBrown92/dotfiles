{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wall #-}

import Control.Lens ((^?))
import Data.Aeson (eitherDecode)
import Data.Aeson.Key (fromString)
import Data.Aeson.Lens (AsValue (_String), key)
import Data.ByteString.Lazy.Char8 (pack)
import Data.Text (unpack)
import Data.Yaml (Value)
import System.Process (readCreateProcess, shell)

main :: IO ()
main = do
  response <- getGPU
  let maybeGPU = response ^? key (fromString "card0") . key "GPU memory use (%)" . _String
  case maybeGPU of
    Just gpu -> putStrLn $ unpack gpu
    Nothing -> putStrLn ""

getGPU :: IO Value
getGPU = do
  jsonResult <- readCreateProcess (shell "rocm-smi --showmemuse --json") ""
  case eitherDecode (pack jsonResult) of
    Right value -> return value
    Left err -> fail err
