{ pkgs, ... }:
{
  haskellProjects.default = {
    devShell = {
      enable = true;

      tools = hp: {
        inherit (hp)
          cabal-fmt
          haskell-language-server
          ;
        inherit (pkgs)
          alejandra
          just
          ;
      };

      hlsCheck.enable = true;
    };
  };
}
