{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wall #-}

module EWWLib where

import Control.Concurrent (threadDelay)
import Control.Exception (evaluate)
import Data.Aeson (FromJSON (parseJSON), Value, eitherDecode, withObject, (.:))
import Data.Aeson.Types (Parser)
import Data.ByteString.Lazy (hGetContents)
import Data.Maybe (listToMaybe)
import Network.Socket (Family (AF_UNIX), SockAddr (SockAddrUnix), SocketType (Stream), connect, defaultProtocol, socket, socketToHandle)
import System.Environment (getEnv)
import System.IO (Handle, IOMode (ReadMode), hClose, hGetLine)
import System.Process (CreateProcess (std_in, std_out), StdStream (CreatePipe), proc, readCreateProcess, shell, withCreateProcess)
import Text.Parsec (parse)
import Text.Parsec.String (Parser)
import Prelude hiding (sin)

getVariableValue :: String -> IO String
getVariableValue var = filter (/= '\n') <$> readCreateProcess (shell $ "eww get " ++ var) ""

update :: [(String, String)] -> [String]
update = ("update" :) . map (\(var, val) -> var ++ "=" ++ val)

-- Functions:
getMons :: IO (Maybe [Monitor])
getMons = either (const Nothing) Just <$> readProcessJSON "hyprctl" ["monitors", "-j"]

getWSs :: IO (Maybe [Workspace])
getWSs = either (const Nothing) Just <$> readProcessJSON "hyprctl" ["workspaces", "-j"]

readProcessJSON :: (FromJSON a) => FilePath -> [String] -> IO (Either String a)
readProcessJSON cmd args =
  withCreateProcess (proc cmd args) {std_in = CreatePipe, std_out = CreatePipe} readAndDecode
  where
    readAndDecode (Just sin) (Just sout) _ _ = do
      hClose sin
      str <- hGetContents sout
      evaluate (eitherDecode str)
    readAndDecode _ _ _ _ = error "Invalid handle configuration for readAndDecode."

getFocusedMon :: [Monitor] -> Maybe Monitor
getFocusedMon mons = listToMaybe [x | x <- mons, monFocused x]

dispatch :: String -> [String] -> [String]
dispatch dis = (["dispatch", dis] ++)

readHyprlandSocketOutput :: Text.Parsec.String.Parser (Maybe a) -> (Maybe a -> IO ()) -> IO ()
readHyprlandSocketOutput parser successIO = do
  xdgEnv <- getEnv "XDG_RUNTIME_DIR"
  hisEnv <- getEnv "HYPRLAND_INSTANCE_SIGNATURE"
  let sockPath = xdgEnv ++ "/hypr/" ++ hisEnv ++ "/.socket2.sock"
  sock <- socket AF_UNIX Stream defaultProtocol
  connect sock (SockAddrUnix sockPath)
  handle <- socketToHandle sock ReadMode
  successIO Nothing
  loop parser successIO handle

loop :: Text.Parsec.String.Parser (Maybe a) -> (Maybe a -> IO ()) -> Handle -> IO ()
loop parser successIO handle = do
  line <- hGetLine handle
  case parse parser "" line of
    Left _ -> pure ()
    Right maybeA -> do
      successIO maybeA
      threadDelay 800000
  loop parser successIO handle

-- Datatypes:
data Monitor = Monitor
  { monActiveWSID :: Int,
    monActiveWSName :: String,
    monFocused :: Bool,
    monID :: Int,
    monName :: String
  }
  deriving (Show)

instance FromJSON Monitor where
  parseJSON :: Value -> Data.Aeson.Types.Parser Monitor
  parseJSON = withObject "Monitor" $ \o -> do
    activeWS <- o .: "activeWorkspace"
    activeWSId <- activeWS .: "id"
    activeWSName <- activeWS .: "name"
    Monitor activeWSId activeWSName
      <$> o
        .: "focused"
      <*> o
        .: "id"
      <*> o
        .: "name"

data Workspace = Workspace
  { wsID :: Int,
    wsName :: String
  }
  deriving (Eq, Ord, Show)

instance FromJSON Workspace where
  parseJSON :: Value -> Data.Aeson.Types.Parser Workspace
  parseJSON = withObject "Workspace" $ \o ->
    Workspace <$> o .: "id" <*> o .: "name"
