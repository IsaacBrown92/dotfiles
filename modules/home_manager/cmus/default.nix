{ pkgs, ... }:
{
  home.packages = builtins.attrValues { inherit (pkgs) cmus; };

  xdg.configFile."cmus/catppuccin_mocha.theme".source = ./catppuccin_mocha.theme;
}
