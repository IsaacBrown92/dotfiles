{
  inputs,
  pkgs,
  ...
}:
let
  stable = inputs.nixpkgs-stable.legacyPackages.${pkgs.system};
in
{
  home.packages = builtins.attrValues {
    inherit (pkgs)
      # ardour
      # gimp-with-plugins
      inkscape
      libreoffice
      ;

    inherit (pkgs.kdePackages)
      kdenlive
      ;

    # inherit
    #   (stable)
    # ; # These randomly broke, version it back up whenever you find yourself back in this file.
  };
}
