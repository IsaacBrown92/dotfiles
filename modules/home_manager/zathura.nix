{ flake, ... }:
let
  inherit (flake.config.aesthetics.themes.${flake.config.aesthetics.currentTheme}) colors;
  makeColor = c: "#" + c;
in
{
  programs.zathura = {
    enable = true;
    options = {
      default-fg = makeColor colors.text;
      default-bg = makeColor colors.base;

      completion-bg = makeColor colors.surface0;
      completion-fg = makeColor colors.text;
      completion-highlight-bg = makeColor colors.surface2;
      completion-highlight-fg = makeColor colors.text;
      completion-group-bg = makeColor colors.surface0;
      completion-group-fg = makeColor colors.blue;

      statusbar-fg = makeColor colors.text;
      statusbar-bg = makeColor colors.surface0;

      notification-bg = makeColor colors.surface0;
      notification-fg = makeColor colors.text;
      notification-error-bg = makeColor colors.surface0;
      notification-error-fg = makeColor colors.red;
      notification-warning-bg = makeColor colors.surface0;
      notification-warning-fg = makeColor colors.peach;

      inputbar-fg = makeColor colors.text;
      inputbar-bg = makeColor colors.surface0;

      recolor-lightcolor = makeColor colors.base;
      recolor-darkcolor = makeColor colors.text;

      index-fg = makeColor colors.text;
      index-bg = makeColor colors.base;
      index-active-fg = makeColor colors.text;
      index-active-bg = makeColor colors.surface0;

      render-loading-bg = makeColor colors.base;
      render-loading-fg = makeColor colors.text;

      highlight-color = makeColor colors.surface2;
      highlight-fg = makeColor colors.pink;
      highlight-active-color = makeColor colors.pink;
    };
  };
}
