{
  programs.brave = {
    enable = true;
    extensions = [
      { id = "naepdomgkenhinolocfifgehidddafch"; } # Browserpass
      { id = "bkkmolkhemgaeaeggcmfbghljjjoofoh"; } # Catppuccin Mocha
      { id = "gafhhkghbfjjkeiendhlofajokpaflmk"; } # Lace
      { id = "hdhinadidafjejdhmfkjgnolgimiaplp"; } # Read Aloud
      { id = "hipekcciheckooncpjeljhnekcoolahp"; } # Tabliss
      { id = "dbepggeogbaibhgnhhndojpepiihcmeb"; } # Vimium
    ];
  };
}
