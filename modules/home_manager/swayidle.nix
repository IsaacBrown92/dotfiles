{
  lib,
  pkgs,
  ...
}:
{
  services.swayidle = {
    enable = true;
    events = [
      {
        event = "after-resume";
        command = lib.getExe pkgs.swaylock;
      }
    ];
    systemdTarget = "hyprland-session.target";
  };
}
