{
  inputs,
  pkgs,
  flake,
  ...
}:
{
  gtk = {
    cursorTheme = {
      name = "catppuccin-mocha-dark-cursors";
      package = pkgs.catppuccin-cursors.mochaDark;
    };

    enable = true;

    font = {
      name = flake.config.aesthetics.font;
      # package = pkgs.nerdfonts.override {fonts = ["VictorMono"];};
      package = inputs.nixpkgs-gtk.legacyPackages.${pkgs.system}.nerdfonts.override {
        fonts = [ "VictorMono" ];
      };
    };

    iconTheme = {
      package = pkgs.catppuccin-papirus-folders.override {
        flavor = "mocha";
        accent = "blue";
      };
      name = "Papirus-Dark";
    };

    theme = {
      package = pkgs.catppuccin-gtk.override {
        size = "compact";
        variant = "mocha";
      };
      name = "catppuccin-mocha-blue-compact";
    };
  };
}
