{
  flake,
  pkgs,
  inputs,
  ...
}:
let
  inherit (flake.config.people) myself;
in
{
  programs.firefox = {
    enable = true;
    package = pkgs.firefox;
    profiles.${myself} = {
      bookmarks = [
        {
          name = "Home Manager Options";
          url = "https://nix-community.github.io/home-manager/options.xhtml";
        }
        {
          name = "Instacart";
          url = "https://instacart.ca";
        }
        {
          name = "Moonlander Configuration";
          url = "https://configure.zsa.io/moonlander/layouts/3aVbB/latest/0";
        }
        {
          name = "Nextcloud";
          url = "https://nextcloud.askyourself.ca";
        }
        {
          name = "OPNsense";
          url = "https://10.0.0.1:8443";
        }
        {
          name = "OpenWrt";
          url = "https://10.0.7.2";
        }
        {
          name = "Syncthing";
          url = "localhost:8384";
        }
      ];
      extensions.packages = builtins.attrValues {
        inherit (inputs.firefox-addons.packages.${pkgs.system})
          browserpass
          darkreader
          firefox-color
          sponsorblock
          ublock-origin
          vimium
          ;
      };
      search = {
        default = "Brave";
        engines = {
          "AlternativeTo" = {
            definedAliases = [ "@at" ];
            icon = ./icons/alternativeto.png;
            urls = [ { template = "https://alternativeto.net/browse/search/?q={searchTerms}"; } ];
          };
          "Amazon" = {
            definedAliases = [ "@am" ];
            icon = ./icons/amazon.png;
            urls = [ { template = "https://www.amazon.ca/s?k={searchTerms}&ref=nav_bb_sb"; } ];
          };
          "Brave" = {
            definedAliases = [ "@br" ];
            icon = ./icons/brave.png;
            urls = [ { template = "https://search.brave.com/search?q={searchTerms}&source=web"; } ];
          };
          "Elm Packages" = {
            definedAliases = [ "@el" ];
            icon = ./icons/brave.png;
            urls = [ { template = "https://search.brave.com/search?q={searchTerms}&source=web"; } ];
          };
          "KissAnime" = {
            definedAliases = [ "@ka" ];
            icon = ./icons/kissanime.ico;
            urls = [ { template = "https://kissanime.com.ru/Search/?s={searchTerms}"; } ];
          };
          "Library Genesis" = {
            definedAliases = [ "@lg" ];
            icon = ./icons/library-genesis.svg;
            urls = [ { template = "https://libgen.gs/index.php?req={searchTerms}"; } ];
          };
          "Nix Packages - Packages" = {
            definedAliases = [ "@npp" ];
            icon = ./icons/nix.png;
            urls = [
              {
                template = "https://search.nixos.org/packages?channel=unstable&size=50&sort=relevance&type=packages&query={searchTerms}";
              }
            ];
          };
          "Nix Packages - Options" = {
            definedAliases = [ "@npo" ];
            icon = ./icons/nix.png;
            urls = [
              {
                template = "https://search.nixos.org/options?channel=unstable&size=50&sort=relevance&type=packages&query={searchTerms}";
              }
            ];
          };
          "NixOS Wiki" = {
            definedAliases = [ "@nw" ];
            icon = ./icons/nix.png;
            urls = [ { template = "https://nixos.wiki/index.php?search={searchTerms}"; } ];
          };
          "The Pirate Bay" = {
            definedAliases = [ "@pb" ];
            icon = ./icons/the-pirate-bay.png;
            urls = [
              {
                template = "https://thepiratebay.org/search.php?q={searchTerms}&all=on&search=Pirate+Search&page=0&orderby=";
              }
            ];
          };
          "Pool - Handle" = {
            definedAliases = [ "@plh" ];
            icon = ./icons/pool.png;
            urls = [ { template = "https://pool.pm/{searchTerms}"; } ];
          };
          "Pool - Other" = {
            definedAliases = [ "@plo" ];
            icon = ./icons/pool.png;
            urls = [ { template = "https://pool.pm/search/{searchTerms}"; } ];
          };
          "Stanford Encyclopedia of Philosophy" = {
            definedAliases = [ "@sep" ];
            icon = ./icons/stanford-encyclopedia-of-philosophy.png;
            urls = [ { template = "https://plato.stanford.edu/search/search?query={searchTerms}"; } ];
          };
          "Internet Encyclopedia of Philosophy" = {
            definedAliases = [ "@iep" ];
            icon = ./icons/internet-encyclopedia-of-philosophy.png;
            urls = [
              {
                template = "https://cse.google.com/cse?cx=001101905209118093242%3Arsrjvdp2op4&ie=UTF-8&q={searchTerms}&sa=Search";
              }
            ];
          };
          "Urban Dictionary" = {
            definedAliases = [ "@ud" ];
            icon = ./icons/urban-dictionary.png;
            urls = [ { template = "https://www.urbandictionary.com/define.php?term={searchTerms}"; } ];
          };
          "Wikipedia" = {
            definedAliases = [ "@wk" ];
            icon = ./icons/wikipedia.ico;
            urls = [ { template = "https://en.wikipedia.org/wiki/{searchTerms}"; } ];
          };
          "YouTube" = {
            definedAliases = [ "@yt" ];
            icon = ./icons/youtube.png;
            urls = [ { template = "https://www.youtube.com/results?search_query={searchTerms}"; } ];
          };
          "Amazon.ca".metaData.hidden = true;
          "Bing".metaData.hidden = true;
          "DuckDuckGo".metaData.hidden = true;
          "Google".metaData.hidden = true;
          "eBay".metaData.hidden = true;
          "Wikipedia (en)".metaData.hidden = true;
        };
        force = true;
        order = [
          "AlternativeTo"
          "Amazon"
          "Brave"
          "KissAnime"
          "Library Genesis"
          "Nix Packages - Packages"
          "Nix Packages - Options"
          "NixOS Wiki"
          "The Pirate Bay"
          "Pool - Handle"
          "Pool - Other"
          "Stanford Encyclopedia of Philosophy"
          "Internet Encyclopedia of Philosophy"
          "Urban Dictionary"
          "Wikipedia"
          "YouTube"
        ];
      };
      settings = {
        "browser.aboutConfig.showWarning" = false;
        "browser.aboutwelcome.enabled" = false;
        "browser.newtabpage.activity-stream.feeds.system.topstories" = false;
        "browser.newtabpage.activity-stream.feeds.topsites" = false;
        "browser.tabs.firefox-view" = false;
        "browser.tabs.tabmanager.enabled" = false;
        "browser.toolbars.bookmarks.visibility" = "never";
        "browser.startup.page" = 3;
        "extensions.pocket.enabled" = false;
        "font.name.monospace.x-western" = "VictorMono Nerd Font Mono";
        "font.name.sans-serif.x-western" = "VictorMono Nerd Font";
        "font.name.serif.x-western" = "VictorMono Nerd Font";
        "security.webauth.u2f" = true;
        "signon.rememberSignons" = false;
        "toolkit.legacyUserProfileCustomizations.stylesheets" = true;

        "extensions.formautofill.creditCards.enabled" = false; # check if it prompts to save card next time I buy online
        # "signon.autofillForms" = false;
      };
      userChrome = builtins.readFile ./userChrome.css;
    };
  };
}
