{
  programs.newsboat = {
    enable = true;
    extraConfig = "include ~/.config/newsboat/themes/catppuccin_mocha";
    urls = [
      {
        tags = [ ];
        url = "https://rss.samharris.org/feed/470b6c2e-d307-4ddf-96e2-1b3bbd68f565";
      }
      {
        tags = [ ];
        url = "https://announcements.askyourself.ca";
      }
      # {
      #   tags = [];
      #   url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCWnSP49DJf-G9E0VfqDdBCA";
      # }
    ];
  };
  xdg.configFile."newsboat/themes/catppuccin_mocha".source = ./catppuccin_mocha;
}
