{ pkgs, ... }:
{
  home.packages = builtins.attrValues {
    inherit (pkgs)
      brave
      # claude-desktop-fhs
      # freecad
      # grafana
      kid3
      ledger-live-desktop
      # monero-gui
      nitrokey-app
      nitrokey-app2
      syncplay
      teams-for-linux
      tor-browser
      trezor-suite
      wally-cli
      wireshark
      zotero
      #school
      # nomachine-client
      # opera
      google-chrome
      ;
    # inherit (pkgs.rocmPackages) rocm-smi;
  };
}
