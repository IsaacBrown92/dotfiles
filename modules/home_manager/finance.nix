{ pkgs, ... }:
{
  home.packages = builtins.attrValues {
    inherit (pkgs)
      hledger
      hledger-ui
      ;
  };
}
