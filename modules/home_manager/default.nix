{ self, ... }:
let
  # inherit (import ../helpers.nix) mkModuleList;
  modules = self.lib.mkModuleList ./.;
in
{
  flake = {
    homeModules = {
      commandline = {
        imports = [
          modules.bat
          modules.bottom
          modules.direnv
          modules.git
          modules.gitui
          modules.gpg
          modules.helix
          modules.home_manager
          modules.kitty
          modules.lazygit
          modules.command_line
          modules.nixvim
          modules.nushell
          modules.password_store
          modules.starship
          modules.yazi
          modules.yt_dlp
          modules.zellij
          modules.zoxide
        ];
      };
      communication = {
        imports = [
          modules.communication
          modules.iamb
          modules.thunderbird
        ];
      };
      desktop = {
        imports = [
          modules.desktop
          modules.ghostty
          modules.himalaya
          modules.hyprland
          modules.newsboat
          modules.zed
        ];
      };
      editing = {
        imports = [
          modules.editing
        ];
      };
      finance = {
        imports = [
          modules.finance
        ];
      };
      gaming = {
        imports = [
          modules.gaming
        ];
      };
      graphicalBase = {
        imports = [
          modules.browserpass
          modules.cursor
          modules.dunst
          modules.easy_effects
          modules.eww
          modules.gammastep
          modules.graphical_base
          modules.gtk
          modules.network_manager_applet
          modules.swayidle
          modules.swaylock
          modules.tofi
          modules.wezterm
          modules.zathura
        ];
      };
      internet = {
        imports = [
          modules.firefox
        ];
      };
      laptop = {
        imports = [
          # modules.hyprland_laptop -- fix me
          modules.laptop
        ];
      };
      media = {
        imports = [
          modules.cmus
          modules.media
          modules.mpv
          modules.obs
          modules.qbittorrent
        ];
      };
      programming = {
        imports = [
          modules.vscodium
        ];
      };
      productivity = {
        imports = [
          modules.productivity
        ];
      };
      services = {
        imports = [
          modules.syncthing
        ];
      };
    };
  };
}
