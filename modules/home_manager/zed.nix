{ pkgs, ... }:
{
  programs.zed-editor = {
    enable = true;
    extraPackages = builtins.attrValues {
      inherit (pkgs)
        nil
        nixd
        tinymist
        ;
    };
    extensions = [
      "ada"
      "catppuccin"
      # "catppuccin-blur"
      # "cargo-appraiser"
      # "cargo-tom"
      # "markdown-oxide"
      # "material-icon"
      # "rose-pine-theme"
      # "material-theme"
      # "just"
      # "elm"
      # "haskell"
      "nix"
      "typst"
      # "nu"
      # "toml"
      # "xml"
    ];
    # userKeymaps = userKeymapsPath;
    userSettings = {
      assistant = {
        enabled = true;
        default_model = {
          provider = "anthropic";
          model = "claude-3-5-sonnet-latest";
        };
        version = "2";
      };
      autosave = "on_focus_change";
      cursor_blink = true;
      load_direnv = "shell_hook";

      lsp = {
        tinymist.initialization_options = {
          exportPdf = "onSave";
        };
        nil.formatting.command = "nixfmt";
      };
      relative_line_numbers = true;
      show_whitespaces = "none";
      # terminal.env = {
      #   TERM = "xterm-256color";
      # };
      theme = "Catppuccin Mocha";
      # vim_mode = false;
    };
  };
}
