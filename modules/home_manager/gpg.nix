{ pkgs, ... }:
{
  programs.gpg = {
    enable = true;
    settings = {
      default-key = "isaac";
    };
  };

  services.gpg-agent = {
    enable = true;
    # extraConfig = "allow-loopback-pienetry";
    pinentryPackage = pkgs.pinentry-curses;
  };
}
