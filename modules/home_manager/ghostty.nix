{
  programs.ghostty = {
    enable = true;
    settings = {
      confirm-close-surface = false;
      theme = "catppuccin-mocha";
      window-decoration = false;
    };
  };
}
