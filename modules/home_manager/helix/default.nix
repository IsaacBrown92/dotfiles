{
  lib,
  pkgs,
  ...
}:
{
  programs.helix = {
    enable = true;
    languages = {
      language = [
        {
          auto-format = true;
          formatter = {
            args = [ "pretty" ];
            command = lib.getExe pkgs.gnat14;
          };
          name = "ada";
        }
        {
          auto-format = true;
          formatter.command = "shfmt";
          name = "bash";
        }
        {
          auto-format = true;
          language-servers = [ "elm-language-server" ];
          name = "elm";
        }
        {
          auto-format = true;
          formatter.command = lib.getExe pkgs.fprettify;
          name = "fortran";
          roots = [ "flake.nix" ];
        }
        {
          auto-format = true;
          name = "haskell";
          roots = [ "flake.nix" ];
        }
        {
          auto-format = true;
          formatter = {
            args = [ "-" ];
            command = lib.getExe pkgs.google-java-format;
          };
          language-servers = [ pkgs.jdt-language-server.pname ];
          name = "java";
        }
        {
          name = "markdown";
          language-servers = [ "vale" ];
        }
        {
          auto-format = true;
          formatter.command = "nixfmt";
          language-servers = [ "nil" ];
          name = "nix";
        }
        {
          auto-format = true;
          formatter = {
            args = [ "-s" ];
            command = "nufmt";
          };
          name = "nu";
          roots = [ "flake.nix" ];
        }
        {
          auto-format = true;
          formatter = {
            args = [
              "--stdin"
              "foo.rb"
              "-a"
              "--stderr"
              "--fail-level"
              "fatal"
              "--server"
            ];
            command = "rubocop";
          };
          name = "ruby";
        }
        {
          auto-format = true;
          name = "rust";
          roots = [ "flake.nix" ];
        }
        {
          file-types = [ "txt" ];
          language-servers = [ "vale" ];
          name = "text";
          scope = "text.plain";
        }
        {
          auto-format = true;
          name = "toml";
        }
        {
          auto-format = true;
          language-id = "typescriptreact"; # This is important for Motion Canvas.
          name = "tsx";
        }
        {
          auto-format = true;
          name = "typescript";
        }
        {
          auto-format = true;
          formatter.command = "typstyle";
          language-servers = [ "tinymist" ];
          name = "typst";
        }
        {
          auto-format = true;
          formatter = {
            args = [ "-in" ];
            command = "yamlfmt";
          };
          name = "yaml";
        }
      ];
      language-server = {
        elm-language-server = {
          config.elmLS = {
            disableElmLSDiagnostics = true;
            elmReviewDiagnostics = "warning";
          };
        };
        "${pkgs.jdt-language-server.pname}" = {
          command = lib.getExe pkgs.jdt-language-server;
        };
        tinymist = {
          command = lib.getExe pkgs.tinymist;
          config.exportPdf = "onType";
        };
        vale = {
          command = lib.getExe pkgs.vale-ls;
        };
        # gpt = {
        #   command = "helix-gpt";
        # };
      };
    };
    # package = inputs.helix.packages.${pkgs.system}.default;
    settings = {
      editor = {
        auto-format = true;
        auto-save = true;
        line-number = "relative";
        soft-wrap.enable = true;
      };
      keys = {
        normal = {
          space = {
            f = ":format";
            q = ":q";
            w = ":w";
          };
        };
      };
      theme = "catppuccin_mocha";
    };
  };
}
