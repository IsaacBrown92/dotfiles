{
  programs.yazi = {
    enable = true;
    enableNushellIntegration = true;
    keymap.manager.prepend_keymap = [
      {
        on = [ "Q" ];
        run = "quit";
      }
      {
        on = [ "q" ];
        run = "quit --no-cwd-file";
      }
    ];
    settings.manager = {
      sort_by = "natural";
      sort_dir_first = true;
      sort_reverse = false;
    };
  };
}
