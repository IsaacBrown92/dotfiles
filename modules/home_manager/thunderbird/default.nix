{
  programs.thunderbird = {
    enable = true;
    profiles = { };
  };

  xdg.configFile."thunderbird/themes".source = ./themes;
}
