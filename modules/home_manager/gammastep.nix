{
  # Remember that this uses geoclue2, which is in ../../nixos/modules.
  services.gammastep = {
    enable = true;
    provider = "geoclue2";
    temperature = {
      day = 5500;
      night = 3000;
    };
  };
}
