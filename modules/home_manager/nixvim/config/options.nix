{
  programs.nixvim.opts = {
    clipboard = "unnamedplus";
    relativenumber = true;
    wrap = true;
  };
}
