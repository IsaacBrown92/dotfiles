{
  programs.nixvim.plugins.web-devicons = {
    autoLoad = true;
    enable = true;
  };
}
