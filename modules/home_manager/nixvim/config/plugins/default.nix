{
  imports = builtins.filter (path: baseNameOf path != "default.nix") (
    builtins.map (name: ./${name}) (builtins.attrNames (builtins.readDir ./.))
  );
}
