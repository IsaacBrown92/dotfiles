{ pkgs, ... }:
{
  home.packages = builtins.attrValues {
    inherit (pkgs) iamb;
  };
  xdg.configFile."iamb/config.json".source = ./config.json;
}
