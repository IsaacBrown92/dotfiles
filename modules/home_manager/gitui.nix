{ flake, ... }:
let
  inherit (flake.config.aesthetics.themes.${flake.config.aesthetics.currentTheme}) colors;
  makeSome = c: "Some(\"#" + c + "\")";
in
{
  programs.gitui = {
    enable = true;
    theme = ''
      (
          branch_fg: ${makeSome colors.teal},
          cmdbar_bg: ${makeSome colors.mantle},
          cmdbar_extra_lines_bg: ${makeSome colors.mantle},
          command_fg: ${makeSome colors.text},
          commit_author: ${makeSome colors.sapphire},
          commit_hash: ${makeSome colors.lavender},
          commit_time: ${makeSome colors.subtext1},
          danger_fg: ${makeSome colors.red},
          diff_file_removed: ${makeSome colors.maroon},
          diff_file_modified: ${makeSome colors.peach},
          diff_file_moved: ${makeSome colors.mauve},
          diff_line_add: ${makeSome colors.green},
          diff_file_added: ${makeSome colors.green},
          diff_line_delete: ${makeSome colors.red},
          disabled_fg: ${makeSome colors.overlay1},
          push_gauge_bg: ${makeSome colors.blue},
          push_gauge_fg: ${makeSome colors.base},
          selection_bg: ${makeSome colors.surface2},
          selection_fg: ${makeSome colors.text},
          selected_tab: Some("Reset"),
          tag_fg: ${makeSome colors.rosewater}
      )
    '';
  };
}
