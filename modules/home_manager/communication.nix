{ pkgs, ... }:
{
  home.packages = builtins.attrValues {
    inherit (pkgs)
      discord
      element-desktop
      signal-desktop
      vesktop
      xdg-utils # Currently needed to open links from Discord.
      ;
  };
}
