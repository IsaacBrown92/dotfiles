{ flake, ... }:
let
  inherit (flake.config.aesthetics.themes.${flake.config.aesthetics.currentTheme}) colors;
  makeColor = c: "#" + c;
in
{
  services.dunst = {
    enable = true;
    settings = {
      global = {
        inherit (flake.config.aesthetics) font;
        background = makeColor colors.base;
        corner_radius = 8;
        fade_in_duration = 1000;
        foreground = makeColor colors.text;
        frame = 10000;
        frame_color = makeColor colors.blue;
        frame_width = 4;
        icon_corner_radius = 4;
        monitor = 1;
        offset = "30x30";
        origin = "bottom-right";
        progress_bar_corner_radius = 4;
        timeout = 10;
        transparecncy = true;
      };

      urgency_critical = {
        frame_color = makeColor colors.peach;
        timeout = 0;
      };
    };
  };
}
