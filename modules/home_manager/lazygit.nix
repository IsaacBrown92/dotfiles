{ flake, ... }:
let
  inherit (flake.config.aesthetics.themes.${flake.config.aesthetics.currentTheme}) colors;
  makeSome = c: "#" + c;
in
{
  programs.lazygit = {
    enable = true;
    settings.gui.theme = {
      activeBorderColor = [
        "${makeSome colors.blue}"
        "bold"
      ];
      inactiveBorderColor = [ "${makeSome colors.subtext0}" ];
      optionsTextColor = [ "${makeSome colors.blue}" ];
      selectedLineBgColor = [ "${makeSome colors.surface0}" ];
      cherryPickedCommitBgColor = [ "${makeSome colors.surface1}" ];
      cherryPickedCommitFgColor = [ "${makeSome colors.blue}" ];
      unstagedChangesColor = [ "${makeSome colors.red}" ];
      defaultFgColor = [ "${makeSome colors.text}" ];
      searchActiveBorderColor = [ "${makeSome colors.yellow}" ];
      authorColors = [ "${makeSome colors.lavender}" ];
    };
  };
}
