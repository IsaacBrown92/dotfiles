{ pkgs, ... }:
{
  home.packages = builtins.attrValues {
    inherit (pkgs)
      qpwgraph
      vlc
      ;
  };
}
