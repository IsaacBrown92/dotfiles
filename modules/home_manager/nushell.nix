{
  programs.nushell = {
    configFile.text = ''
      sleep 7ms
      $env.config = {show_banner: false}

      $env.ANTHROPIC_API_KEY = (open /run/secrets/anthropic-key)
    '';
    enable = true;
    shellAliases = {
      e = "edgedb";
      g = "gitui";
      h = "hledger";
      n = "nvim";
      la = "ls -la";
      lg = "lazygit";
      ll = "ls -l";
      tb = "nc termbin.com 9999";
      y = "yy";
      yt = "yt-dlp";
      ze = "zellij";
    };
  };
}
