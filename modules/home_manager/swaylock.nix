{
  flake,
  pkgs,
  ...
}:
let
  inherit (flake.config.aesthetics.themes.${flake.config.aesthetics.currentTheme}) colors;
in
{
  home.packages = builtins.attrValues { inherit (pkgs) swaylock; };

  programs.swaylock.settings = {
    inherit (flake.config.aesthetics) font;
    indicator-idle-visible = true;
    indicator-radius = 100;
    indicator-thickness = 20;
    show-failed-attempts = true;

    bs-hl-color = colors.red;
    color = colors.base;
    key-hl-color = colors.mauve;

    caps-lock-bs-hl-color = colors.red;
    caps-lock-key-hl-color = colors.mauve;

    inside-color = colors.base;
    inside-clear-color = colors.base;
    inside-caps-lock-color = colors.base;
    inside-ver-color = colors.base;
    inside-wrong-color = colors.base;

    line-color = colors.base;
    line-clear-color = colors.base;
    line-caps-lock-color = colors.base;
    line-ver-color = colors.base;
    line-wrong-color = colors.base;

    ring-color = colors.crust;
    ring-clear-color = colors.crust;
    ring-caps-lock-color = colors.crust;
    ring-ver-color = colors.crust;
    ring-wrong-color = colors.crust;

    separator-color = "00000000";

    text-color = colors.text;
    text-clear-color = colors.text;
    text-caps-lock-color = colors.text;
    text-ver-color = colors.text;
    text-wrong-color = colors.text;
  };
}
