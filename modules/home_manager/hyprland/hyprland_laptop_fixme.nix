{
  flake,
  pkgs,
  ...
}:
{
  home.packages = builtins.attrValues {
    inherit (pkgs)
      grim
      hyprpaper
      pulsemixer
      slurp
      wl-clipboard
      ;
  };

  wayland.windowManager.hyprland = {
    enable = true;
    settings =
      let
        inherit (flake.config.aesthetics.themes.${flake.config.aesthetics.currentTheme}) colors;
        makeColor = c: "0xff" + c;
      in
      {
        animation = [
          "border, 1, 5, default"
          "fade, 1, 5, default"
          "windows, 1, 5, default"
          "windowsOut, 1, 5, default"
          "workspaces, 1, 5, default"
        ];

        bind =
          builtins.map (x: "SUPER, " + x) [
            "B, exec, firefox"
            "G, exec, logseq"
            "Return, exec, ghosttyy "
            "Space, exec, tofi-drun"
            "H, exec, ghosttyy -e nu -e y"
            "M, exec, ghosttyy -e cmus"
            "U, exec, ghosttyy -e nu -e numbat"
            "F, exec, ghosttyy -e zellij a Dotfiles"
            "L, exec, swaylock"
            "S,  exec, grim -g \"$(slurp)\" - | wl-copy -t image/png"

            "C, fullscreen, 0"
            "K, killactive,"
            "T, togglefloating"

            "period, splitratio, +0.1"
            "slash, splitratio, -0.1"

            "N, movefocus, l"
            "E, movefocus, d"
            "O, movefocus, u"
            "I, movefocus, r"

            "1, workspace, 1"
            "2, workspace, 2"
            "3, workspace, 3"
            "4, workspace, 4"
            "5, workspace, 5"
            "6, workspace, 6"
            "7, workspace, 7"
            "8, workspace, 8"
            "9, workspace, 9"
            "0, workspace, 10"
          ]
          ++ builtins.map (x: "SUPER ALT SHIFT CTRL, " + x) [
            "S, exec, grim -g \"$(slurp)\""

            "N, movewindow, l"
            "E, movewindow, d"
            "O, movewindow, u"
            "i, movewindow, r"

            "1, movetoworkspace, 1"
            "2, movetoworkspace, 2"
            "3, movetoworkspace, 3"
            "4, movetoworkspace, 4"
            "5, movetoworkspace, 5"
            "6, movetoworkspace, 6"
            "7, movetoworkspace, 7"
            "8, movetoworkspace, 8"
            "9, movetoworkspace, 9"
            "0, movetoworkspace, 10"
          ];

        decoration = {
          # "active_opacity" = "0.86";
          # "drop_shadow" = "no";
          # "inactive_opacity" = "0.7";
          "rounding" = "10";
        };

        exec-once = "~/.config/hypr/binaries/initialize";

        general = {
          "border_size" = "4";
          "col.active_border" = builtins.concatStringsSep " " [
            (makeColor colors.blue)
            (makeColor colors.sky)
            "45deg"
          ];
          "col.inactive_border" = makeColor colors.crust;
          "gaps_in" = "2";
          "gaps_out" = "4";
        };

        input."follow_mouse" = "2";

        layerrule = "blur, monitorBarGPU";

        monitor = [
          "eDP-1,1920x1080@144,auto,1"
        ];

        # windowrule = builtins.map (x: "opaque, " + x) [
        #   "^(.gimp-2.10-wrapped_)$"
        #   "^(Brave-browser)$"
        #   "^(com.obsproject.Studio)$"
        #   "^(firefox)$"
        #   "^(FreeCAD)$"
        #   "^(Google-chrome)$"
        #   "^(libreoffice-writer)$"
        #   "^(mpv)$"
        #   "^(org.inkscape.Inkscape)$"
        #   "^(org.kde.kdenlive)$"
        #   "^(org.kde.okular)$"
        #   "^(org.pwmt.zathura)$"
        #   "^(teams-for-linux)$"
        #   "^(thunderbird)$"
        #   "^(virt-manager)$"
        #   "^(vlc)$"
        # ];
      };
  };

  xdg.configFile = {
    "hypr/hyprpaper.conf".source = ../hyprland/hyprpaper/laptop.conf;
    "hypr/binaries".source = ./binaries;
    "hypr/wallpapers".source = ./hyprpaper/wallpapers;
  };
}
