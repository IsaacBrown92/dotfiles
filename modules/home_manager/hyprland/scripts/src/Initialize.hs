{-# LANGUAGE BlockArguments #-}
{-# OPTIONS_GHC -Wall #-}

import Network.HostName (getHostName)
import System.Process (callCommand, callProcess)

main :: IO ()
main = do
  hostname <- getHostName
  -- callCommand "swaylock"
  -- callCommand "ze -s main"
  callProcess "eww" $
    "open"
      : case hostname of
        "desktop" -> ["monitorBarGPU"]
        _ -> ["monitorBar"]
  callCommand "hyprpaper"
