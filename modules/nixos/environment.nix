{ pkgs, ... }:
{
  environment = {
    enableAllTerminfo = false;
    variables = {
      DIRENV_LOG_FORMAT = "";
      EDITOR = "hx";
      GRIM_DEFAULT_DIR = "$HOME/Pictures/screenshots/"; # Should not be on non-graphical systems
      LEDGER_FILE = "/run/media/isaac/crypt/.hledger.journal"; # Should only be on Desktop.
      PASSWORD_STORE_DIR = "/run/media/isaac/crypt/passwords";
      PASSWORD_STORE_ENABLE_EXTENSIONS = "true";
      WLR_NO_HARDWARE_CURSORS = "1"; # Should not be on non-graphical systems
    };
    systemPackages = builtins.attrValues {
      inherit (pkgs)
        git
        pijul
        sshfs
        virt-manager
        ;
    };
  };
}
