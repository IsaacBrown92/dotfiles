{
  flake,
  pkgs,
  ...
}:
{
  users = {
    users.${flake.config.people.myself} = {
      extraGroups = [
        "dialout"
        "docker"
        "kvm"
        "libvirtd"
        "networkmanager"
        "wheel"
        "wireshark"
      ];
      isNormalUser = true;
      shell = pkgs.nushell;
    };
  };
}
