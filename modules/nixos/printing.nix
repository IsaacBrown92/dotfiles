{
  hardware.printers = {
    ensurePrinters = [
      {
        name = "Lexmark-MC3326adwe";
        location = "Home";
        deviceUri = "https://10.0.5.2";
        model = "drv:///sample.drv/generic.ppd";
      }
    ];
    ensureDefaultPrinter = "Lexmark-MC3326adwe";
  };

  services.printing.enable = true;
}
