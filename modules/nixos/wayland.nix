{ pkgs, ... }:
# let
# toInstall = builtins.attrValues {inherit (pkgs) sway;};
# in
{
  environment.sessionVariables.NIXOS_OZONE_WL = "1";
  # environment.systemPackages = toInstall;
  # services.displayManager.sessionPackages = toInstall;
  xdg = {
    portal = {
      enable = true;
      extraPortals = builtins.attrValues {
        inherit (pkgs)
          xdg-desktop-portal-hyprland
          xdg-desktop-portal-wlr
          xdg-desktop-portal-gtk
          ;
        inherit (pkgs.kdePackages)
          xdg-desktop-portal-kde
          ;
      };
      wlr.enable = true;
      xdgOpenUsePortal = true;
    };
  };
}
