{
  inputs,
  lib,
  pkgs,
  ...
}:
let
  inherit (inputs.edgedb.packages."x86_64-linux") edgedb-server;
  bootstrap-server = pkgs.writeText "bootstrap.edgesql" ''
    ALTER ROLE edgedb SET password := "test";
  '';
  working-dir = "/var/lib/edgedb";
in
{
  environment.systemPackages = [
    pkgs.edgedb
  ];

  users.groups.edgedb = { };
  users.users.edgedb = {
    group = "edgedb";
    description = "EdgeDB Server";
    isSystemUser = true;
    packages = [
      edgedb-server
    ];
  };

  system.activationScripts = {
    edgedb-data-dir = {
      text = ''
        install -d -m 0750 -o edgedb -g edgedb ${working-dir}
      '';
    };
  };

  systemd.services = {
    "edgedb@" = {
      enable = true;
      after = [
        "network.target"
        "syslog.target"
      ];
      description = "EdgeDB Database Service, instance %i";
      documentation = [ "https://edgedb.com" ];
      path = [ edgedb-server ];
      reload = "${pkgs.coreutils}/bin/kill -HUP $MAINPID";
      script = ''
        ${lib.getExe' edgedb-server "edgedb-server"} \
        --data-dir=${working-dir}/$1 \
        --disable-dynamic-system-config \
        --emit-server-status=${working-dir}/$1/status.json \
        --instance-name=$1 \
        --security=insecure_dev_mode \
        --tls-cert-mode=generate_self_signed \
        --admin-ui=enabled
      '';
      scriptArgs = "%i";
      serviceConfig = {
        Type = "simple";
        User = "edgedb";
        Group = "edgedb";
        KillMode = "mixed";
        TimeoutSec = 0;
        ExecStartPre = ''
          ${lib.getExe' edgedb-server "edgedb-server"} \
          --data-dir=${working-dir}/%i \
          --instance-name=%i \
          --disable-dynamic-system-config \
          --bootstrap-only \
          --bootstrap-command-file=${bootstrap-server}
        '';
      };
    };
    "edgedb@test" = {
      enable = true;
      environment = {
        EDGEDB_DEBUG_HTTP_INJECT_CORS = "1";
      };
      overrideStrategy = "asDropin";
    };
  };
}
