{ self, ... }:
let
  # inherit (import ../helpers.nix) mkModuleList;
  modules = self.lib.mkModuleList ./.;
in
{
  flake = {

    nixosModules = {
      base = {
        imports = [
          modules.doas
          modules.environment
          modules.nix
          modules.user
        ];
      };
      graphicalBase = {
        imports = [
          modules.geoclue2
          modules.hyprland
          modules.plasma
          modules.sound
          modules.swaylock
          modules.wayland
        ];
      };
      desktop = {
        imports = [
          modules.edgedb
          modules.locale
          modules.obs_scene_switcher
          modules.printing
          modules.steam
          modules.trezor
          modules.virtualisation
        ];
      };
      laptop = {
        imports = [
          modules.bluetooth
          modules.edgedb
          modules.locale
          modules.printing
        ];
      };
      homeServer1 = {
        imports = [
          modules.locale
        ];
      };
      homeServer2 = {
        imports = [
          modules.locale
          modules.nextcloud
        ];
      };
    };
  };
}
