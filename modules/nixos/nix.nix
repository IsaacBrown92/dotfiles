{ inputs, ... }:
{
  nix = {
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 10d";
    };
    registry.nixpkgs.flake = inputs.nixpkgs;
    settings = {
      extra-deprecated-features = [ "url-literals" ];
      extra-experimental-features = [
        "flakes"
        "nix-command"
      ];
      substituters = [ "https://hyprland.cachix.org" ];
      trusted-public-keys = [ "hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc=" ];
    };
  };
  nixpkgs.config.allowUnfree = true;
}
