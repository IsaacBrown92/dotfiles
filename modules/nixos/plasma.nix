{
  services = {
    desktopManager = {
      plasma6 = {
        enable = true;
      };
    };

    displayManager = {
      sddm = {
        enable = true;
        enableHidpi = true;

        # settings = {
        #   Autologin = {
        #     Session = "plasma.desktop";
        #     User = "sof";
        #   };
        # };

        wayland.enable = true;
      };
    };
  };
}
