{
  config,
  flake,
  pkgs,
  ...
}:
let
  inherit (flake.config.people) myself;
  inherit (flake.config.people.users.${myself}) domain;
in
{
  services = {
    nextcloud = {
      autoUpdateApps.enable = true;
      config = {
        adminpassFile = config.sops.secrets.nextcloud-password.path;
        dbtype = "pgsql";
      };
      configureRedis = true;
      database.createLocally = true;
      enable = true;
      hostName = domain;
      # hostName = "10.0.4.2";
      https = true;
      package = pkgs.nextcloud29;
      phpOptions."opcache.interned_strings_buffer" = "16";
      settings = {
        client_max_body_size = "20G";
        default_phone_region = "CA";
        log_type = "file";
        mail_smtpmode = "sendmail";
        mail_sendmailmode = "pipe";
        maintenance_window_start = 4;
        overwriteprotocol = "https";
        # trusted_domains = ["10.0.2.2"];
      };
    };

    nginx = {
      enable = true;
      virtualHosts.${domain} = {
        forceSSL = true;
        sslCertificate = config.sops.secrets.website-ssl-cert.path;
        sslCertificateKey = config.sops.secrets.website-ssl-key.path;
      };
    };
  };
}
